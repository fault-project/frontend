import React, { useState } from 'react';

interface Props {
  onChange: (e: boolean) => void;
  value?: boolean;
}

const Toggle: React.FC<Props> = ({ onChange, value = false }) => {
  return (
    <label className='switch'>
      <input
        type='checkbox'
        checked={value}
        onChange={() => onChange(!value)}
      />
      <span className='slider round'></span>
    </label>
  );
};

export default Toggle;
