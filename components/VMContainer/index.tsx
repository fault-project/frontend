import React from 'react';
import { MachineProps } from '../../pages';
import styles from '../../styles/ProjectsContainer.module.scss';

import VMGridItem from './VMGridItem';

interface Props {
  machines: MachineProps[];
}

const ProjectsContainer: React.FC<Props> = (props) => {
  return (
    <div className={styles.container}>
      <h3 className={styles.heading}>&gt; active machines</h3>
      <div className={styles.gridContainer}>
        {props.machines.map((elt, i) => (
          <VMGridItem key={i} machine={elt} />
        ))}
      </div>
    </div>
  );
};

export default ProjectsContainer;
