import Link from 'next/link';
import React from 'react';
import { MachineProps } from '../../pages';
import styles from '../../styles/ProjectsContainer.module.scss';
import { Colors } from '../../utils/constants';
import ComputerIcon from '../ComputerIcon/ComputerIcon';

interface Props {
  machine: MachineProps;
}

const VMGridItem: React.FC<Props> = (props) => {
  const random = Math.floor(Math.random() * Colors.length);
  return (
    <Link href={`/machine/${props.machine.device_uuid}`}>
      <div className={styles.vmGridItem}>
        <ComputerIcon fill={Colors[random]} />
        <div className={styles.details}>
          <h4 className={styles.tag}>name: {props.machine.device_name}</h4>
          <h4 className={styles.tag}>creator: {props.machine.creator_id}</h4>
        </div>
      </div>
    </Link>
  );
};

export default VMGridItem;
