import clsx from 'clsx';
import React from 'react';
import styles from './StatCard.module.scss';

interface Props {
  value: number;
}

const StatCard: React.FC<Props> = (props) => {
  return (
    <div className={clsx(styles.statContainer, 'box-shadow ')}>
      <div className={styles.number}>{props.value}</div>
      <div className={styles.title}>Faults Detected</div>
    </div>
  );
};

export default StatCard;
