import React from 'react';
import styles from './StatCard.module.scss';

const StatContainer: React.FC = ({ children }) => {
  return <div className={styles.statsContainer}>{children}</div>;
};

export default StatContainer;
