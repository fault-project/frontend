import React, { useEffect, useReducer, useState } from 'react';
import styles from '../../styles/Functions.module.scss';
import { ALAG_SERVICE_KA_URL } from '../../utils/constants';
import Toggle from '../Toggle/Toggle';

interface FunctionsProps {
  machineID: string;
  onClose: () => void;
}

const MachineFunctions: React.FC<FunctionsProps> = (props) => {
  const [list, setList] = useState([]);
  const [used, setUsed] = useState(new Set());

  useEffect(() => {
    (async () => {
      const data = await (
        await fetch(ALAG_SERVICE_KA_URL + 'fetch/functions/all')
      ).json();

      try {
        const used = await (
          await fetch(
            ALAG_SERVICE_KA_URL +
              'fetch/functions/machine?machine_id=' +
              props.machineID
          )
        ).json();
        setUsed(new Set(used));
      } catch (err) {
        setUsed(new Set());
      }

      setList(data);
    })();
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.innerContainer}>
        {list.length ? <></> : <>loading...</>}
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            marginBottom: '1rem',
            cursor: 'pointer',
          }}
          onClick={props.onClose}
        >
          <span className='material-icons'>close</span>
        </div>
        {list.map((item) => (
          <FunctionListItem
            key={item}
            data={item}
            selected={used.has(item)}
            onClick={(e) => {
              e
                ? setUsed((state) => {
                    state.add(item);

                    fetch(ALAG_SERVICE_KA_URL + 'map/function-to-machine', {
                      method: 'POST',
                      body: JSON.stringify({
                        machineId: props.machineID,
                        functionId: item,
                      }),
                    }).catch((err) => console.log(err));

                    return new Set(state);
                  })
                : setUsed((state) => {
                    state.delete(item);

                    fetch(
                      ALAG_SERVICE_KA_URL +
                        'delete/machine/functions?machine_id=' +
                        props.machineID +
                        '&function_id=' +
                        item
                    ).catch((err) => console.log(err));

                    return new Set(state);
                  });
            }}
          />
        ))}
      </div>
    </div>
  );
};

interface FunctionListItemProps {
  data: string;
  selected: boolean;
  onClick: (e: boolean) => void;
}

const FunctionListItem: React.FC<FunctionListItemProps> = (props) => {
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: '0.5rem',
        border: '1px solid black',
        padding: '0.25rem 1rem',
      }}
    >
      <div style={{}}>{props.data}</div>
      <Toggle onChange={props.onClick} value={props.selected} />
    </div>
  );
};

export default MachineFunctions;
