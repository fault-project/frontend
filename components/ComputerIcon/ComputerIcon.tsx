import React from 'react';

interface ComputerIconProps {
  fill: string;
}

const ComputerIcon: React.FC<ComputerIconProps> = (props) => {
  return (
    <div style={{ background: props.fill }}>
      <svg viewBox='0 0 128 128' xmlns='http://www.w3.org/2000/svg'>
        <rect
          x='28'
          y='29.5'
          width='53'
          height='47.5'
          rx='7.5'
          fill='white'
          stroke='black'
        />
        <rect
          x='37.5'
          y='68.5'
          width='63'
          height='30'
          rx='7.5'
          fill='white'
          stroke='black'
        />
      </svg>
    </div>
  );
};

export default ComputerIcon;
