import React, { useEffect, useReducer, useState } from 'react';
import styles from '../../styles/Functions.module.scss';
import { ALAG_SERVICE_KA_URL } from '../../utils/constants';

const initialState: Array<string> = [];

const FunctionsReducer = (
  state: Array<string>,
  action: { type: string; payload: any }
) => {
  switch (action.type) {
    case 'ADD':
      return [...state, action.payload];
    case 'DELETE':
      return state.filter((item) => item !== action.payload);
    case 'UPDATE':
      return [
        ...state.filter((item) => item !== action.payload),
        action.payload,
      ];
    case 'FLOOD':
      return [...action.payload];
    default:
      return [...state];
  }
};

interface FunctionsProps {
  onClose: () => void;
}

const Functions: React.FC<FunctionsProps> = (props) => {
  const [api, setAPI] = useState('');
  const [text, setText] = useState('');
  const [name, setName] = useState('');
  const [range, setRange] = useState('1');

  const [list, dispatch] = useReducer(FunctionsReducer, initialState);

  useEffect(() => {
    (async () => {
      const data = await (
        await fetch(ALAG_SERVICE_KA_URL + 'fetch/functions/all')
      ).json();
      dispatch({ type: 'FLOOD', payload: data });
    })();
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.innerContainer}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            marginBottom: '1rem',
            cursor: 'pointer',
          }}
          onClick={props.onClose}
        >
          <span className='material-icons'>close</span>
        </div>
        {list.map((item) => (
          <FunctionListItem
            key={item}
            data={item}
            onDelete={() => {
              fetch(ALAG_SERVICE_KA_URL + 'delete/function?functionId=' + item)
                .then((data) => data.json())
                .catch((err) => console.log(err));

              dispatch({ type: 'DELETE', payload: item });
            }}
          />
        ))}

        <div
          style={{
            display: 'flex',
            marginBottom: '0.5rem',
          }}
        >
          <input
            style={{
              outline: 'none',
              padding: '0.25rem',
              marginRight: '1rem',
              border: 'none',
              borderBottom: '1px solid black',
              flex: 1,
              marginBottom: '0.5rem',
              width: '100%',
            }}
            value={api}
            onChange={(e) => setAPI(e.target.value)}
            placeholder='function api here'
          />
          <div
            style={{
              margin: '0.4rem',
              cursor: 'pointer',
              display: 'grid',
              placeItems: 'center',
            }}
            onClick={() => {
              dispatch({ type: 'ADD', payload: name });
              setText('');
              setRange('');
              setAPI('');
              setName('');
              fetch(ALAG_SERVICE_KA_URL + 'set/function', {
                method: 'POST',
                body: JSON.stringify({
                  functionId: name,
                  uri: text,
                  priority: range,
                  'api-key': api,
                }),
              });
            }}
          >
            <span className='material-icons'>done</span>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            marginBottom: '0.5rem',
          }}
        >
          <input
            style={{
              outline: 'none',
              padding: '0.25rem',
              marginRight: '1rem',
              border: 'none',
              borderBottom: '1px solid black',
              flex: 1,
              marginBottom: '0.5rem',
            }}
            value={text}
            onChange={(e) => setText(e.target.value)}
            placeholder='function url here'
          />
          <input
            style={{
              outline: 'none',
              padding: '0.25rem',
              marginRight: '1rem',
              border: 'none',
              borderBottom: '1px solid black',
              flex: 1,
              marginBottom: '0.5rem',
            }}
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder='function name here'
          />
          <div
            style={{ display: 'flex', alignItems: 'center', margin: '1rem' }}
          >
            <input
              type='range'
              min={1}
              max={10}
              value={range}
              onChange={(e) => setRange(e.target.value)}
            />
            <p style={{ marginLeft: '1rem', width: '2ch' }}>{range}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

interface FunctionListItemProps {
  data: string;
  onDelete: () => void;
}

const FunctionListItem: React.FC<FunctionListItemProps> = (props) => {
  return (
    <div style={{ display: 'flex', marginBottom: '0.5rem' }}>
      <input
        style={{
          outline: 'none',
          padding: '0.25rem',
          marginRight: '1rem',
          border: 'none',
          borderBottom: '1px solid black',
          flex: 1,
        }}
        disabled
        value={props.data}
        placeholder='function url here'
      />
      <div
        style={{
          margin: '0.4rem',
          cursor: 'pointer',
          display: 'grid',
          placeItems: 'center',
        }}
        onClick={props.onDelete}
      >
        <span className='material-icons'>delete_outline</span>
      </div>
    </div>
  );
};

export default Functions;
