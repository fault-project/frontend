import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  ResponsiveContainer,
} from 'recharts';
import { Colors } from '../../utils/constants';

const ErrorChart = () => {
  const data = [
    { name: 'Page A', uv: 400, pv: 240, amt: 2400 },
    { name: 'Page B', uv: 500, pv: 280, amt: 2400 },
    { name: 'Page C', uv: 600, pv: 300, amt: 2400 },
    { name: 'Page D', uv: 750, pv: 500, amt: 2400 },
    { name: 'Page E', uv: 600, pv: 20, amt: 2400 },
    { name: 'Page F', uv: 300, pv: 400, amt: 2400 },
    { name: 'Page G', uv: 800, pv: 360, amt: 2400 },
    { name: 'Page H', uv: 850, pv: 240, amt: 2400 },
    { name: 'Page I', uv: 540, pv: 240, amt: 2400 },
  ];

  return (
    <ResponsiveContainer>
      <LineChart data={data} margin={{ left: -20, right: 10 }}>
        <Line type='monotone' dataKey='uv' stroke={Colors[0]} strokeWidth={2} />
        <Line type='monotone' dataKey='pv' stroke={Colors[5]} strokeWidth={2} />
        <CartesianGrid stroke='#ececec' />
        <XAxis dataKey='name' fontSize={12} fontFamily='Poppins' />
        <YAxis fontSize={12} fontFamily='Poppins' />
      </LineChart>
    </ResponsiveContainer>
  );
};

export default ErrorChart;
