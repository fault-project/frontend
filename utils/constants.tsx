export const Colors = [
  '#ff9aa2',
  '#ffb7b2',
  '#c7ceea',
  '#b5ead7',
  '#ffdac1',
  '#ffee93',
];

const BASE_URL = "http://ec2-13-234-113-120.ap-south-1.compute.amazonaws.com" 

export const NGROK = BASE_URL + ':5000/';

export const ALAG_SERVICE_KA_URL = BASE_URL + ':5002/';
