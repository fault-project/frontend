import '../styles/globals.scss';
import type { AppProps } from 'next/app';
import Link from 'next/link';
import '../styles/Toggle.scss';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <div className='container'>
      <nav>
        <Link href='/'>
          <h1 className='logo'>dashboard</h1>
        </Link>
      </nav>
      <Component {...pageProps} />
    </div>
  );
}

export default MyApp;
