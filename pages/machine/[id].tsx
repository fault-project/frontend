import type { GetServerSideProps, NextPage } from 'next';
import Head from 'next/head';
import { useState } from 'react';
import { MachineProps } from '..';

import ErrorChart from '../../components/ErrorChart/ErrorChart';
import MachineFunctions from '../../components/MachineFunctions/MachineFunctions';
import { StatCard, StatContainer } from '../../components/StatCards';
import Toggle from '../../components/Toggle/Toggle';
import styles from '../../styles/Machine.module.scss';
import { NGROK } from '../../utils/constants';

interface Props {
  data: boolean[];
  machine: MachineProps;
  logs: string;
  errorLogs: string;
  errors: number;
}

const Machine: NextPage<Props> = (props) => {
  const [showFunctions, setShowFunctions] = useState(false);
  const [state, setState] = useState({
    'bot-stat': props.data[0],
    'email-stat': props.data[1],
  });

  return (
    <div>
      <Head>
        <title>👽 Machine ID</title>
        <meta name='description' content='Log Project' />
      </Head>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <button
          style={{
            outline: 'none',
            background: 'none',
            border: '1px solid black',
            padding: '0.25rem 1rem',
            cursor: 'pointer',
          }}
          onClick={() => setShowFunctions(true)}
        >
          Show Functions
        </button>
      </div>
      <h5 className={styles.machineID}>
        Machine ID: <span>{props.machine.device_uuid}</span>
      </h5>
      <h5 className={styles.machineID}>
        Machine Name: <span>{props.machine.device_name}</span>
      </h5>
      <h5 className={styles.machineID}>
        Creator: <span>{props.machine.creator_id}</span>
      </h5>
      <br />
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'start',
        }}
      >
        <div style={{ margin: '1rem' }}>
          <Toggle
            value={state['bot-stat']}
            onChange={(e) => {
              setState((state) => ({ ...state, 'bot-stat': e }));
              fetch(NGROK + 'web/set/device/alarm', {
                method: 'POST',
                body: JSON.stringify({
                  'device-id': props.machine.device_uuid,
                  ...state,
                  'bot-stat': e,
                }),
              });
            }}
          />
        </div>
        <div>Send Notifications from Bot</div>
      </div>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'start',
        }}
      >
        <div style={{ margin: '1rem' }}>
          <Toggle
            value={state['email-stat']}
            onChange={(e) => {
              setState((state) => ({ ...state, 'email-stat': e }));
              fetch(NGROK + 'web/set/device/alarm', {
                method: 'POST',
                body: JSON.stringify({
                  'device-id': props.machine.device_uuid,
                  ...state,
                  'email-stat': e,
                }),
              });
            }}
          />
        </div>
        <div>Send Notifications from Email</div>
      </div>
      <br />
      <StatContainer>
        <StatCard value={props.errors} />
      </StatContainer>
      <div className={styles.chartContainer}>
        <div>
          <ErrorChart />
        </div>
        <div>
          <ErrorChart />
        </div>
      </div>
      <div className={styles.logContainer}>
        <label hidden>logs</label>
        <h5 className={styles.logHeading}>latest logs</h5>
        <textarea
          className={styles.logs}
          value={props.logs}
          rows={20}
          disabled
        />
      </div>
      <div className={styles.logContainer}>
        <label hidden>error logs</label>
        <h5 className={styles.logHeading}>error logs</h5>
        <textarea
          className={styles.logs}
          value={props.errorLogs}
          rows={20}
          disabled
        />
      </div>
      {showFunctions && (
        <MachineFunctions
          machineID={props.machine.device_uuid}
          onClose={() => setShowFunctions(false)}
        />
      )}
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;
  const res = await fetch(NGROK + 'web/fetch/device/info?deviceId=' + id);
  const machine = await res.json();

  const data = await (
    await fetch(NGROK + 'web/fetch/device/alarm?uuid=' + id)
  ).json();

  const logsRes = await (
    await fetch(NGROK + 'web/fetch/machine-logs?device_uuid=' + id)
  ).json();

  let logs = '';
  logsRes.forEach((elt: string[]) => {
    logs += `${elt[0]} ${elt[1]} \n`;
  });

  const errorLogsRes = await (
    await fetch(NGROK + 'web/fetch/error-logs?device_uuid=' + id)
  ).json();
  let errorLogs = '';
  errorLogsRes.forEach((elt: string[]) => {
    errorLogs += `${elt[0]} ${elt[1]} ${elt[2]} \n`;
  });

  return {
    props: {
      machine,
      data: data === -1 ? [false, false] : data,
      logs,
      errorLogs,
      errors: errorLogsRes.length,
    },
  };
};

export default Machine;
