import type { GetServerSideProps, NextPage } from 'next';
import Head from 'next/head';
import { useState } from 'react';
import ErrorChart from '../components/ErrorChart/ErrorChart';
import Functions from '../components/Functions/Functions';
// import { StatCard, StatContainer } from '../components/StatCards';
import VMContainer from '../components/VMContainer';
import styles from '../styles/Home.module.scss';
import { NGROK } from '../utils/constants';

export interface MachineProps {
  creator_id: string;
  device_name: string;
  device_uuid: string;
}

export interface Props {
  machines: MachineProps[];
}

const Home: NextPage<Props> = (props) => {
  const [showFunctions, setShowFunctions] = useState(false);

  return (
    <div>
      <Head>
        <title>👽 Dashboard</title>
        <meta name='description' content='Log Project' />
      </Head>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <button
          style={{
            outline: 'none',
            background: 'none',
            border: '1px solid black',
            padding: '0.25rem 1rem',
            cursor: 'pointer',
          }}
          onClick={() => setShowFunctions(true)}
        >
          Show Functions
        </button>
      </div>
      <div className={styles.chartContainer}>
        <div>
          <ErrorChart />
        </div>
        <div>
          <ErrorChart />
        </div>
      </div>
      {/* <StatContainer>
        <StatCard />
        <StatCard />
        <StatCard />
        <StatCard />
      </StatContainer> */}
      <VMContainer machines={props.machines} />
      {showFunctions && <Functions onClose={() => setShowFunctions(false)} />}
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (_) => {
  const res = await fetch(NGROK + 'web/fetch/devices/info/all');
  const data = await res.json();
  const machines = Object.keys(data).map((el) => data[el]);

  return { props: { machines } };
};

export default Home;
