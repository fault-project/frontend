import { NextPage } from 'next';
import Head from 'next/head';
import Functions from '../components/Functions/Functions';

const Function: NextPage = () => {
  return (
    <>
      <div>
        <Head>
          <title>👽 Dashboard</title>
          <meta name='description' content='Log Project' />
        </Head>
      </div>
      <Functions />
    </>
  );
};

export default Function;
