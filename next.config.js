/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['source.unsplash.com', 'pbs.twimg.com'],
  },
};

module.exports = nextConfig;
